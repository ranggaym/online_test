<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User management API.
 *
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Soal extends Admin_Controller
{

	public function jawaban()
	{
		$this->load->model('jawaban_model');
		echo $this->jawaban_model->datatable();
	}

	public function kunci_jawaban()
	{
		$this->load->model('kunci_jawaban_model');
		echo $this->kunci_jawaban_model->datatable();
	}

	public function peserta()
	{
		$this->load->model('peserta_model');
		echo $this->peserta_model->datatable();
	}

	public function index()
	{
		$this->load->model('soal_model');
		echo $this->soal_model->datatable();
	}
}
