<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User management API.
 *
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Test extends Admin_Controller
{
  function __construct()
	{
		parent::__construct();

		$this->load->model('test/test_model');
	}

	public function index()
	{
		echo $this->test_model->datatable();
	}
}
