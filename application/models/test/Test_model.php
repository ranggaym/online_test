<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User Model
 *
 * @package App
 * @category Model
 * @author Ardi Soebrata
 */
class Test_model extends MY_Model {

	protected $table = 'test_1';
	protected $role_table = 'acl_roles';
	private $ci;

	function __construct()
	{
		parent::__construct();
		// $this->ci = & get_instance();
		// $this->ci->load->library('PasswordHash', array('iteration_count_log2' => 8, 'portable_hashes' => FALSE));
	}




	/**
	 * Check if username is available
	 *
	 * @param string $username
	 * @param int $id
	 * @return boolean
	 */
	function is_username_unique($username, $id = 0)
	{
		$this->db->where('username', $username);
		if ($id > 0)
			$this->db->where($this->id_field . ' <>', $id);
		$query = $this->db->get($this->table);
		return ($query->num_rows() == 0);
	}

	/**
	 * Check if email is available
	 *
	 * @param string $email
	 * @param int $id
	 * @return boolean
	 */
	function is_email_unique($email, $id = 0)
	{
		$this->db->where('email', $email);
		if ($id > 0)
			$this->db->where($this->id_field . ' <>', $id);
		$query = $this->db->get($this->table);
		return ($query->num_rows() == 0);
	}

	function datatable()
	{
		$this->datatables->select('id, attr_varchar, attr_int, attr_text, attr_file, "" AS role, "" AS registered, "" AS action')
				->from($this->table);
		return $this->datatables->generate();
	}
}
