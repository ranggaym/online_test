<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User Model
 *
 * @package App
 * @category Model
 * @author Ardi Soebrata
 */
class Kunci_jawaban_model extends MY_Model {

	protected $table = 'kunci_jawaban';
	protected $role_table = 'acl_roles';
	private $ci;

	function __construct()
	{
		parent::__construct();
	}




	function datatable()
	{
    $table = $this->table;
		$this->datatables->select('id, pertanyaan, teks_jawaban, skor, "" AS role, "" AS registered, "" AS action')
				->from($table)
        ->join('soal', "soal.id = $table.id_soal")
        ->join('jawaban', "jawaban.id = $table.id_jawaban");
		return $this->datatables->generate();
	}
}
