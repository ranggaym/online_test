<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User Model
 *
 * @package App
 * @category Model
 * @author Ardi Soebrata
 */
class Soal_model extends MY_Model {

	protected $table = 'soal';
	protected $role_table = 'acl_roles';
	private $ci;

	function __construct()
	{
		parent::__construct();
	}





	function datatable()
	{
		$this->datatables->select('id, jenis_soal, pertanyaan, "" AS role, "" AS registered, "" AS action')
				->from($this->table);
		return $this->datatables->generate();
	}
}
