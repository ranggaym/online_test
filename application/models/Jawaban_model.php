<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User Model
 *
 * @package App
 * @category Model
 * @author Ardi Soebrata
 */
class Jawaban_model extends MY_Model {

	protected $table = 'jawaban';
	protected $role_table = 'acl_roles';
	private $ci;

	function __construct()
	{
		parent::__construct();
	}

  function hitung_skor_total_by_peserta($id_peserta)
  {
    $table = $this->table;
    $this->db->select('SUM(skor) AS total_skor')
      ->where('id_peserta', $id_peserta)
      ->join('kunci_jawaban', "kunci_jawaban.id_jawaban = $table.id")
    ;
		$row = $this->db->get($table)->row();

		return $row ? $row->total_skor : FALSE;
  }


  function datatable_total_skor($group_by = 'peserta.id')
  {
    $table = $this->table;
    $this->datatables->select('peserta.nama, SUM(skor) AS total_skor, "" AS role, "" AS registered, "" AS action')
  		->from($table)
      ->join('soal', "soal.id = $table.id_soal")
      ->join('kunci_jawaban', "kunci_jawaban.id_jawaban = $table.id")
      ->join('peserta', "peserta.id = $table.id_peserta")
      ->group_by($group_by)
    ;

    return $this->datatables->generate();
  }

	function datatable($id_soal = false)
	{
    $table = $this->table;
		$this->datatables->select('id, soal.pertanyaan, teks_jawaban, peserta.nama, "" AS role, "" AS registered, "" AS action')
  		->from($table)
      ->join('soal', "soal.id = $table.id_soal")
      ->join('peserta', "peserta.id = $table.id_peserta");

    if ($id_soal)
      $this->datatables->where('id_soal', $id_soal);

		return $this->datatables->generate();
	}
}
